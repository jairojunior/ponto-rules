package br.gov.ce.fortaleza.ponto;

import java.time.LocalTime;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import br.gov.ce.fortaleza.ponto.model.Registro;

public class EscalaSemanalTest {
	
	private static KieServices ks;
	private static KieContainer kContainer;
	private static KieSession kSession;

	@BeforeClass
	public static void setUp() {
		ks = KieServices.Factory.get();
		kContainer = ks.getKieClasspathContainer();
		kSession = kContainer.newKieSession("ksession-rules");
	}

	@Test
	public void primeiraEntradaRegular() {
		Registro registro = new Registro();
		registro.setEntrada(LocalTime.parse("08:01"));
		
		process(registro);
		
		Assert.assertEquals(0L, registro.getSaldo().longValue());		
	}
	
	@Test
	public void possuiOcorrenciaJustificada() {
		Registro registro = new Registro();
		registro.setEntrada(LocalTime.parse("09:00"));
		registro.setOcorrencia(true);
		
		process(registro);
		
		Assert.assertEquals(0L, registro.getSaldo().longValue());		
	}
	
	@Test
	public void semPrimeiraEntrada() {
		Registro registro = new Registro();
		
		process(registro);
		
		Assert.assertEquals(240L, registro.getSaldo().longValue());		
	}
	
	@Test
	public void entradaAntesDoHorario() {
		Registro registro = new Registro();
		
		process(registro);
		
		Assert.assertEquals(240L, registro.getSaldo().longValue());		
	}
	
	@Test
	public void entradaComAtraso() {
		Registro registro = new Registro();
		registro.setEntrada(LocalTime.parse("08:16"));
		
		process(registro);
		
		Assert.assertEquals(16L, registro.getSaldo().longValue());
	}

	private void process(Registro registro) {
		kSession.insert(registro);
		kSession.getAgenda().getAgendaGroup("escalaSemanalEmDSL").setFocus(); 
		kSession.fireAllRules();
	}

}
