package br.gov.ce.fortaleza.ponto.model;

import java.time.LocalDate;
import java.time.LocalTime;

public class Registro implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private LocalTime entrada;
	private LocalTime saida;
	private LocalDate data;

	private Long saldo;

	private boolean ocorrencia;

	public Registro() {
	}

	public java.time.LocalTime getEntrada() {
		return this.entrada;
	}

	public void setEntrada(java.time.LocalTime entrada) {
		this.entrada = entrada;
	}

	public java.time.LocalTime getSaida() {
		return this.saida;
	}

	public void setSaida(java.time.LocalTime saida) {
		this.saida = saida;
	}

	public boolean isOcorrencia() {
		return this.ocorrencia;
	}

	public void setOcorrencia(boolean ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

	public java.time.LocalDate getData() {
		return this.data;
	}

	public void setData(java.time.LocalDate data) {
		this.data = data;
	}

	public java.lang.Long getSaldo() {
		return this.saldo;
	}

	public void setSaldo(java.lang.Long saldo) {
		this.saldo = saldo;
	}

	public Registro(java.time.LocalTime entrada, java.time.LocalTime saida,
			java.time.LocalDate data, java.lang.Long saldo,
			boolean ocorrencia) {
		this.entrada = entrada;
		this.saida = saida;
		this.data = data;
		this.saldo = saldo;
		this.ocorrencia = ocorrencia;
	}

}