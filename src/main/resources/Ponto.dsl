[condition][]Registro de ponto recebido=registro: Registro()
[condition][]- com entrada entre {horaInicial} e {horaFinal}=( entrada >= LocalTime.parse("{horaInicial}") && entrada <= LocalTime.parse("{horaFinal}") )
[condition][]OU=||
[keyword][]quando=when
[keyword][]regra=rule
[keyword][]então=then
[keyword][]fim=end
[condition][]- sem entrada registrada=entrada == null
[condition][]- com entrada antes de {value}=entrada < {value}
[condition][]- com entrada após {value}=entrada > {value}
[consequence][]Saldo é zerado=registro.setSaldo(0);
[consequence][]Saldo é igual a diferença entre {hora1} e {hora2}=registro.setSaldo(Duration.between(LocalTime.parse("{hora1}"), LocalTime.parse("{hora2}")).toMinutes());
[consequence][]Saldo é igual a diferença entre horário de entrada e {value}=registro.setSaldo(Duration.between(registro.entrada, LocalTime.parse("{value}")).toMinutes());
