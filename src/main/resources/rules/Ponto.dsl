[condition][]Registro de ponto recebido=registro: Registro()
[condition][]- com entrada entre {horaInicial} e {horaFinal}=entrada >= LocalTime.parse("{horaInicial}") && entrada <= LocalTime.parse("{horaFinal}").plus(15, ChronoUnit.MINUTES)
[condition][]OU=||
[keyword][]quando=when
[keyword][]regra=rule
[keyword][]então=then
[keyword][]fim=end
[condition][]- sem entrada registrada=entrada == null
[condition][]- com entrada antes de {value:\d{2}:\d{2}}=entrada < LocalTime.parse("{value}")
[condition][]- com entrada após {value:\d{2}:\d{2}}=entrada > LocalTime.parse("{value}")
[condition][]- com ocorrência justificada=ocorrencia
[consequence][]Saldo é zerado=registro.setSaldo(0)
[consequence][]Saldo é a diferença entre {value1} e {value2}=registro.setSaldo(Duration.between(LocalTime.parse("{value1}"), LocalTime.parse("{value2}")).abs().toMinutes())
[consequence][]Saldo de atraso é a diferença entre entrada e {value}=registro.setSaldo(Duration.between(registro.entrada, LocalTime.parse("{value}")).abs().toMinutes())